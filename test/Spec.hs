{-# LANGUAGE OverloadedStrings  #-}

import BakingSoda
import BakingSoda.Test.Parser

import qualified Data.Aeson as A

import Test.Hspec

main :: IO ()
main = do
    parsingBasicTestCase
    parsingGroupTestCase
    parsingTableTestCase

parsingBasicTestCase :: IO ()
parsingBasicTestCase = hspec $ do
    describe "single rule" $ do
      it "test string in an object" $
        parsingByRule modifyRule1 stopRule1 json1 modelEnv1 rule1
        `shouldBe` [ A.String "test1"]
      it "test array in an object" $
        parsingByRule modifyRule1 stopRule1 json1 modelEnv1 rule2
        `shouldBe` [ A.String "test2", A.String "test3"]

parsingGroupTestCase :: IO ()
parsingGroupTestCase = hspec $ do
    describe "group rules" $ do
      it "test group has multip rules" $
        parsingByGroup json1 [] modelEnv1 "tablename" [ tableField1 ]
        `shouldBe` [ ProcessRaw
                       { pTableName = "tablename"
                       , pTableField =
                          [ ProcessRawField {
                             pFieldName = "field name1"
                          ,  pValue = A.String "test2"}]
                       }
                   , ProcessRaw
                       { pTableName = "tablename"
                       , pTableField = 
                          [ ProcessRawField
                             { pFieldName = "field name1"
                             , pValue = A.String "test3"}]
                             }
                   ]

parsingTableTestCase :: IO ()
parsingTableTestCase = hspec $ do
    describe "table" $ do
      it "test table" $
        parsingByTable table1 [] modelEnv1 json1
        `shouldBe` [[ ProcessRaw
                        { pTableName = "table name"
                        , pTableField
                           = [ ProcessRawField
                               { pFieldName = "field name1"
                               , pValue =   A.String "test2"}
                             , ProcessRawField
                               { pFieldName = "field name2"
                               , pValue = A.String "test1"}]}
                    , ProcessRaw
                        { pTableName = "table name"
                        , pTableField
                           = [ ProcessRawField
                               { pFieldName = "field name1"
                               , pValue = A.String "test3"}
                             , ProcessRawField
                               { pFieldName = "field name2"
                               , pValue = A.String "test1"}]}]
                   ]
