# Baking Soda
---

This is the core library of baking soda. see [Baking Soda Introduction](https://9chapters.gitlab.io/devlog/2019/07/09/010_bakingsoda_introduction/) for high level overview. Here is an example of [baking soda client](https://gitlab.com/9chapters/baking-soda-tezos).

## How to build

```
sudo apt install libpq-dev
stack build
```

# TODO

- document for customizing configuations
- redesign/provide DSL
- index update for data change
- monitoring/modeling for tracking change
