{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}

module BakingSoda.Db where

import BakingSoda.DBModels

import Conduit

import Control.Monad.IO.Class  (liftIO, MonadIO)
import Control.Monad.Logger    (MonadLogger, runStderrLoggingT)
import Control.Monad.Reader
import Control.Monad.Trans.Control

import qualified Data.Text as T (Text, unpack, pack, intercalate)

import qualified Data.Pool as P
import qualified Data.Text.Encoding as T
import qualified Data.Aeson as A
import qualified Data.Either as E
import qualified Data.Maybe as M
import qualified Data.Aeson.Types as A
import qualified Data.Vector as V
import qualified Data.ByteString.Lazy as B
import qualified Data.Scientific as S

import Database.Groundhog as G
import Database.Groundhog.Core as G
import Database.Groundhog.Generic as G
import Database.Groundhog.Generic.Sql as G
import Database.Groundhog.Postgresql as G
import Database.Groundhog.Postgresql.Array as G

import System.IO

buildCrowlerModel :: (MonadIO m, MonadUnliftIO m, MonadBaseControl IO m)
       => P.Pool Postgresql ->  m ()
buildCrowlerModel =
    runDbConn (runMigration $ migrate (undefined :: RawData))

connDb :: MonadIO m => String -> Int -> m (P.Pool Postgresql)
connDb = createPostgresqlPool

selectRestrictOffset
  :: (MonadIO m, MonadUnliftIO m, MonadBaseControl IO m)
  => Int -- ^ page
  -> Int -- ^ number of raw
  -> String -- ^ source
  -> P.Pool Postgresql
  -> m [ RawData ]
selectRestrictOffset o n id =
    runDbConn (selectRestrictOffset' n o id)
    where selectRestrictOffset'
            :: Int -- ^ number of raw
            -> Int -- ^ page
            -> String -- ^ source
            -> ReaderT Postgresql IO [ RawData ]
          selectRestrictOffset' n o id =
             select $ (SourceField ==. id) `orderBy` [Asc AutoKeyField] `limitTo` n `offsetBy` o

selectRestrict
  :: (MonadIO m, MonadUnliftIO m, MonadBaseControl IO m)
  => Int -- ^ page
  -> Int -- ^ number of raw
  -> String -- ^ source
  -> P.Pool Postgresql
  -> m [ RawData ]
selectRestrict p n id =
    runDbConn (selectRestrict' n p id)
    where selectRestrict'
            :: Int -- ^ number of raw
            -> Int -- ^ page
            -> String -- ^ source
            -> ReaderT Postgresql IO [ RawData ]
          selectRestrict' n p id =
             select $ (SourceField ==. id) `orderBy` [Asc AutoKeyField] `limitTo` n `offsetBy` (p * n)

storeRowData :: (MonadIO m, MonadUnliftIO m, MonadBaseControl IO m)
             => Int          -- trunc for inserting
             -> P.Pool Postgresql
             -> [ RawData ]
             -> m ()
storeRowData n pool xs =
    runDbConn (truncInsert n xs) pool
    where truncInsert
            :: (PersistEntity a, PersistBackend m, MonadIO m, MonadBaseControl IO m)
            => Int
            -> [ a ]
            -> m ()
          truncInsert n [] = return ()
          truncInsert n xs = do
            liftIO $ putStrLn "inserting ..."
            let (f, b) = splitAt n xs
            insertMany_ f
            truncInsert n b

getInsertList :: (MonadIO m, MonadUnliftIO m, MonadBaseControl IO m, Show a, Read a)
              => P.Pool Postgresql -> [ a ] -> m [ a ]
getInsertList pool xs =
    flip runDbConn pool $ do
        l <- identityNotExist $ map (PersistText . T.pack . show) xs
        fmap (read . T.unpack . fst) <$> traverse fromPersistValues l

toEntityPersistValues'
  :: (PersistBackend m, PersistEntity v)
  => v
  -> m [PersistValue]
toEntityPersistValues' = fmap ($ []) . toEntityPersistValues

proxy :: proxy Postgresql
proxy = error "proxy Postgresql"

insertMany_
    :: (PersistEntity a, PersistBackend m, MonadIO m, MonadBaseControl IO m)
    => [ a ]
    -> m ()
insertMany_ xs = do
      vs <- concatMap tail <$> mapM toEntityPersistValues' xs
      executeRaw False query vs
    where
      query = "INSERT INTO " <> tbName' <> " (" <> fieldNames' <> ")" <>
              " VALUES " <> params'

      tbName' = tbName $ head xs
      fieldNames' = commasJoin $ fieldNames $ head xs
      params' = commasJoin $ map insertDataParams xs

insertMany
    :: (PersistEntity a, PersistBackend m, PrimitivePersistField (AutoKey a), MonadIO m, MonadBaseControl IO m)
    => [ a ]
    -> m [ AutoKey a ]
insertMany xs = do
      vs <- concatMap tail <$> mapM toEntityPersistValues' xs
      q <- queryRaw False query vs
      rawValues <- streamToList q
      liftIO $ print rawValues
      return (fmap decode rawValues)
    where
      decode [ x ] = fromPrimitivePersistValue x
      query = "INSERT INTO " <> tbName' <> " (" <> fieldNames' <> ")" <>
              " VALUES " <> params' <> " returning(id) "

      tbName' = tbName $ head xs
      fieldNames' = commasJoin $ fieldNames $ head xs
      params' = commasJoin $ map insertDataParams xs

identityNotExist
    :: (PersistBackend m, MonadIO m, MonadBaseControl IO m)
    => [ PersistValue ]
    -> m [ [ PersistValue ] ]
identityNotExist xs = do
      q <- queryRaw False query xs
      streamToList q
    where
      query = "SELECT t1.identity FROM ( VALUES " <> params' <> ") as t1(identity) LEFT JOIN " <>
              "raw_data" <> " t2 on t1.identity  = t2.identity WHERE t2.identity IS NULL"
      params' = commasJoin $ map (const "(?)") xs

tbName :: (PersistEntity a, StringLike s) => a -> s
tbName r = mainTableName id $ entityDef proxy r

fieldNames :: (PersistEntity a, StringLike s) => a -> [ s ]
fieldNames r = map (fromString . fst)
             $ constrParams
             $ head
             $ constructors
             $ entityDef proxy r

parameterize :: (PersistEntity a, StringLike s) => a -> [ s ]
parameterize r = map (const "?")
               $ constrParams
               $ head
               $ constructors
               $ entityDef proxy r

insertDataParams :: (PersistEntity a, StringLike s) => a -> s
insertDataParams r =  "(" <> commasJoin (parameterize r) <> ")"

storeProcessRawData :: (MonadIO m, MonadUnliftIO m, MonadBaseControl IO m)
             => Int          -- trunc for inserting
             -> P.Pool Postgresql
             -> [ ProcessRaw ]
             -> m ()
storeProcessRawData n pool xs =
    runDbConn (truncInsert n xs) pool
    where truncInsert
            :: (PersistBackend m, MonadIO m, MonadBaseControl IO m)
            => Int
            -> [ ProcessRaw ]
            -> m ()
          truncInsert n [] = return ()
          truncInsert n xs = do
            liftIO $ putStrLn "inserting ..."
            liftIO $ hFlush stdout

            let (f, b) = splitAt n xs
            insertManyByRaw_ f
            truncInsert n b

insertManyByRaw_
    :: (PersistBackend m)
    => [ ProcessRaw ]
    -> m ()
insertManyByRaw_ ps = do
      let vs = concat persisValues'
      executeRaw False (T.unpack query) vs
    where
      query = "INSERT INTO "
            <> tbName'
            <> " (" <> fieldNames' <> ")"
            <> " VALUES " <> params'
            <> " ON CONFLICT (" <> tbUniqueKey' <> ")"
            <> " DO NOTHING"

      tbName' = pTableName $ head ps
      tbUniqueKey' = pUniqueKey $ head ps
      fields' :: [[ ProcessRawField ]]
      fields' = map pTableField ps
      fieldNames' :: T.Text
      fieldNames' = T.intercalate ","
                  $ map pFieldName
                  $ head fields'
      persisValues' :: [[ PersistValue ]]
      persisValues' = map pValueToEntityPersistValues fields'
      params' :: T.Text
      params' = T.intercalate "," $ map persisValuesToParams persisValues'

persisValuesToParams :: [ PersistValue ] -> T.Text
persisValuesToParams ps
  = "("
  <> T.intercalate "," (map (const "?") ps)
  <> ")"

data ProcessRaw
    = ProcessRaw
    { pTableName  :: T.Text
    , pUniqueKey  :: T.Text
    , pTableField :: [ ProcessRawField ]
    } deriving (Show, Eq)
--
data ProcessRawField
    = ProcessRawField
    { pFieldName    :: T.Text
    , pValue        :: A.Value
    } deriving (Show, Eq)

pValueToPresistValue :: Maybe A.Value -> A.Parser PersistValue
pValueToPresistValue (Just v) = A.parseJSON v
pValueToPresistValue Nothing = return PersistNull

pValueToPresistValue' :: A.Value -> PersistValue
pValueToPresistValue' (A.String t) = PersistText t
pValueToPresistValue' (A.Number n) =
   if fromInteger (floor n) == n
     then PersistInt64 $ floor n
     else PersistDouble $ fromRational $ toRational n
pValueToPresistValue' (A.Bool b) = PersistBool b
pValueToPresistValue' A.Null = PersistNull
pValueToPresistValue' (A.Array a) = PersistText
                                   $ bracket
                                   $ T.intercalate ","
                                   $ V.toList
                                   $ V.map (fromPrimitivePersistValue . pValueToPresistValue') a
                                   where bracket x = "{" <> x <> "}"
pValueToPresistValue' v@(A.Object o) = PersistText $ T.decodeUtf8 $ B.toStrict $ A.encode v

pValueToEntityPersistValues :: [ ProcessRawField ] -> [ PersistValue ]
pValueToEntityPersistValues xs = toPersisValue values
    where values :: [ A.Value ]
          values = map pValue xs
          toPersisValue :: [ A.Value ] -> [ PersistValue ]
          toPersisValue = map pValueToPresistValue'
          errorToNull :: Either String PersistValue -> PersistValue
          errorToNull = E.either (const PersistNull) id
