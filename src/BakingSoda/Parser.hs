{-# LANGUAGE OverloadedStrings  #-}
module BakingSoda.Parser where

import BakingSoda.Model
import BakingSoda.Gen
import BakingSoda.Db

import Control.Monad.IO.Class  (liftIO, MonadIO)

import qualified Data.List as L
import qualified Data.Vector as V
import qualified Data.Text as T
import qualified Data.Maybe as M
import qualified Data.Either as E
import qualified Data.Bool as BL
import qualified Data.Aeson as A
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Base64 as BB
import qualified Data.Set as S
import qualified Data.Hashable as H
import qualified Data.HashMap.Strict as H
import Control.Monad
import Control.Concurrent (threadDelay)
import System.IO

newtype ModelEnv a = ModelEnv (H.HashMap PathHash (H.HashMap Id a))
                deriving (Show, Read)

indexing :: String -- env path
         -> String -- configh path
         -> Int    -- ^ page
         -> Int    -- ^ number
         -> Int    -- ^ trunc for inserting
         -> [ Rule () ] -- ^ stop rule
         -> Int    -- ^ sleep when get full data
         -> Int    -- ^ sleep when lost data in this trunc
         -> String    -- ^ source name
         -> String    -- ^ connect string
         -> String    -- ^ connect string for read
         -> IO ()
indexing e f p n o s d1 d2 id cs csr = do
    putStrLn "parsing model..."
    m <- fmap buildModelEnv <$> readEnv e

    putStrLn "parsing config..."
    c <- configIO f

    pool <- connDb cs 3
    poolRead <- connDb csr 3

    hFlush stdout
    let offset = p * n

    indexing' e f offset n o c m s d1 d2 pool poolRead id
    where indexing' e f offset n o c m s d1 d2 pool poolRead id
            = do
              putStrLn $ "preparing indexing from doc .. "
                       <> show offset <> " ~ " <> show n
              hFlush stdout

              case sequence c of
                Left lc -> hPutStrLn stderr lc
                Right rc ->
                  case m of
                    Left lm -> hPutStrLn stderr lm
                    Right rm -> do
                      let tables = configToTable toSqlDataType rc
                      rs <- liftIO $ selectRestrictOffset offset n id poolRead
                      let (rs', vs) = verifyJsonDecode rs

                      mapM_ (putStrLn <=< jsonDecodeFail) rs'

                      mapM_ (storeProcessRawData o pool)
                        $ getParsedDocs
                        $ parsingDocs vs s rm tables
                      threadDelay d1

                      let fullDatas = length rs == n
                      let nextOffset = offset + length rs

                      unless fullDatas $ do
                        putStrLn $ "only get #doc ..." <> show (length rs)
                        threadDelay d2

                      hFlush stdout
                      indexing' e f nextOffset n o c m s d1 d2 pool poolRead id

getParsedDocs :: [[ (BS.ByteString, [[ ProcessRaw ]]) ]] -> [[ ProcessRaw ]]
getParsedDocs = concatMap concatGroup
  where concatGroup :: [ (BS.ByteString, [[ ProcessRaw ]]) ] -> [[ ProcessRaw ]]
        concatGroup xs = map concat $ L.transpose $ map snd xs

parsingDocs
  :: [ (BS.ByteString, A.Value) ]
  -> [ Rule () ]
  -> ModelEnv VariableType
  -> [ Table ]
  -> [[ (BS.ByteString, [[ ProcessRaw ]]) ]]
parsingDocs vs s e = map processRaws
    where processRaws :: Table -> [ (BS.ByteString, [[ ProcessRaw ]]) ]
          processRaws t = map (fmap (parsingByTable t s e)) vs

parsingByTable :: Table -> [ Rule () ] -> ModelEnv VariableType -> A.Value -> [[ ProcessRaw ]]
parsingByTable t s e a = map (parsingByGroup a s e (tableName t) (tableUniqueKey t)) tfs
  where tfs :: [[ TableField ]]
        tfs = H.elems $ tablefield t

parsingByGroup
  :: A.Value
  -> [ Rule () ]
  -> ModelEnv VariableType
  -> T.Text            -- ^ table name
  -> T.Text            -- ^ unique key of table
  -> [ TableField ]    -- ^ a group
  -> [ ProcessRaw ]
parsingByGroup v s e tb tk ts = map (newProcessRaw tb) $ valueToRaws ts
  where tableFieldToValue :: TableField -> [ (T.Text, A.Value) ]
        tableFieldToValue t = map (\x -> (fieldName t, x)) $ mostValue $ map (parsingByRule (fieldModifyRule t) s v e) $ fieldRule t
        tableFieldsToValue :: [ TableField ] -> [[ (T.Text, A.Value) ]]
        tableFieldsToValue = map tableFieldToValue
        valueToRaws :: [ TableField ] -> [[ (T.Text, A.Value) ]]
        valueToRaws = filter (not . L.null) . map isUniKey . L.transpose . supplyShort . filter (not . L.null) . tableFieldsToValue
        rawsToProcessRawFields :: [ (T.Text, A.Value) ] -> [ ProcessRawField ]
        rawsToProcessRawFields [] = []
        rawsToProcessRawFields ((n,v):xs) = ProcessRawField n v : rawsToProcessRawFields xs
        newProcessRaw :: T.Text -> [ (T.Text, A.Value) ] -> ProcessRaw
        newProcessRaw tb ts = ProcessRaw tb tk $ rawsToProcessRawFields ts
        isUniKey :: [ (T.Text, A.Value) ] -> [ (T.Text, A.Value) ]
        isUniKey xs
          | any (\(t,v) -> t == tk && v /= A.Null) xs = xs
          | otherwise                  = []

supplyShort :: (Show a) => [[ a ]] -> [[ a ]]
supplyShort xs = supplyShort' findMax withLen
  where supplyShort' :: (Show a) => Int -> [(Int, [a])] -> [[ a ]]
        supplyShort' i1 [] = []
        supplyShort' i1 ((i2, x):xs)
          | i1 > i2 = (take i1 $ cycle x) : supplyShort' i1 xs
          | otherwise = x : supplyShort' i1 xs
        withLen = map (\x -> (length x, x)) xs
        findMax :: Int
        findMax = maximum $ map length xs

mostValue :: [[ A.Value ]] -> [ A.Value ]
mostValue vs = snd $ foldr max_ (0, []) mostValue'
    where mostValue' :: [(Int, [ A.Value ])]
          mostValue' = flip zip vs $ map (length . filter (/= A.Null)) vs
          max_ :: (Int, [ A.Value ]) -> (Int, [ A.Value ]) -> (Int, [ A.Value ])
          max_ x@(i,xs) y@(j,ys)
            | i >= j = x
            | otherwise = y

parsingByRule
  :: ModifyRule
  -> [ Rule () ]
  -> A.Value
  -> ModelEnv VariableType
  -> Rule VariableType
  -> [ A.Value ]
parsingByRule m s v@(A.Object o) e (Rule _ p a)
  = M.maybe [ A.Null ] (\l -> parsingByRule m (filterStopRule p s) l e a) $ H.lookup p o

parsingByRule List s v@(A.Array a) e (Cat _ _ (Target z)) = [ v ]
parsingByRule m s v@(A.Array a) e r@(Cat _ x t)
  = concat $ V.toList $ V.map (\tv -> parsingByRule m s tv e t) targetValue
  where (A.Array targetValue) = findCatArray v s e r

parsingByRule _ s v@(A.String _) e (Target (VText _)) = [ v ]
parsingByRule _ s v@(A.Number _) e (Target (VNumber _)) = [ v ]
parsingByRule _ s v@(A.Bool _) e (Target (VBool _)) = [ v ]
parsingByRule _ s A.Null e (Target (VNull _)) = [ A.Null ]
parsingByRule _ s v e (Target _) = [ removeUuid v ]
parsingByRule _ _ _ _ _ = [ A.Null ]

removeUuid :: A.Value -> A.Value
removeUuid v@(A.Object o)
  = A.toJSON
  $ H.map removeUuid
  $ H.delete "parent_uuid"
  $ H.delete "uuid" o
removeUuid v@(A.Array a) = A.toJSON $ V.map removeUuid a
removeUuid v@(A.String t) = v
removeUuid v@(A.Number s) = v
removeUuid v@(A.Bool b) = v
removeUuid v@A.Null = v

findCatArray :: A.Value -> [ Rule () ] -> ModelEnv VariableType -> Rule VariableType -> A.Value
findCatArray o@(A.Array a) s (ModelEnv m) (Cat h c r)
    = M.maybe o (\x -> A.Array $ snd <$> filterByType x typeValuePair) findCatType
    where findCatType :: Maybe VariableType
          findCatType = H.lookup (parentHash h) m >>= H.lookup c
          arrayType :: V.Vector A.Value -> [ Rule () ] -> V.Vector VariableType
          arrayType vs s = fmap (\v -> valueType v s Nothing) vs
          typeValuePair :: V.Vector (VariableType, A.Value)
          typeValuePair = V.zip (arrayType a s) a
          filterByType :: VariableType -> V.Vector (VariableType, A.Value) -> V.Vector (VariableType, A.Value)
          filterByType t = V.filter ((== t) . fst)
findCatArray o _ _ _ = o

parentHash :: BS.ByteString -> BS.ByteString
parentHash h = E.either undefined parentHash $ BB.decode h
  where parentHash r = BB.encode $ BS.intercalate "/" $ init $ BS.split '/' r

buildModelEnv :: (Eq a, H.Hashable a) => Env a -> ModelEnv a
buildModelEnv (Env e) = ModelEnv model
    where envType = e
          envTypeKey = H.keys envType
          envTypeValue = map reverseKeyValue $ H.elems envType
          model =  H.fromList $ zip envTypeKey envTypeValue

reverseKeyValue :: (Eq a, H.Hashable a, Eq b, H.Hashable b) => H.HashMap a b -> H.HashMap b a
reverseKeyValue h = H.fromList $ zip vs ks
    where ks = H.keys h
          vs = H.elems h
