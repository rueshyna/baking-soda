module BakingSoda.UtilTH where

import Control.Monad
import Data.Foldable
import Data.Traversable
import Language.Haskell.TH
import Database.Groundhog.TH

-- uncurry1 :: (a -> b -> c) -> (a, b) -> c
-- uncurry2 :: \ f (x1, x2) -> f x1 x2
-- uncurry3 :: \ f (x1, x2, x3) -> f x1 x2 x3
-- uncurry4 :: \ f (x1, x2, x3, x4) -> f x1 x2 x3 x4
-- ...
uncurryN :: Int -> Q Exp
uncurryN n = do
  f  <- newName "f"
  xs <- replicateM n (newName "x")
  let ntup = VarP f : [TupP (map VarP xs)]
      args = VarE f : map VarE xs
  return $ LamE ntup (foldl1 AppE args)

codegenConfig :: CodegenConfig
codegenConfig = defaultCodegenConfig { namingStyle = lowerCaseSuffixNamingStyle }
