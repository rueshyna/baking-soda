{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE GADTs              #-}
{-# LANGUAGE DeriveGeneric      #-}

module BakingSoda.Model where

import BakingSoda.Db
import BakingSoda.DBModels

import Control.Monad.State
import Codec.Binary.UTF8.String as U

import qualified Data.Either as E
import qualified Data.List as L
import qualified Data.Bifunctor as BF
import qualified Data.Bitraversable as BT
import qualified Data.Set as S
import qualified Data.Hashable as H
import qualified Data.HashMap.Strict as H

import qualified Data.Text as T
import qualified Data.ByteString as BS
import qualified Data.ByteString.Base64 as BB
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.UTF8 as B
import qualified Data.Vector as V
import qualified Data.Aeson as A
import qualified Data.Maybe as M

import qualified Text.Layout.Table as TL

import System.IO

import GHC.Generics (Generic)

type PathHash = BS.ByteString
type Path = T.Text
type Id = Int
newtype Env a = Env (H.HashMap PathHash (H.HashMap a Id))
                deriving (Show, Read)

newtype Group = Group Int
                deriving (Read, Generic, Eq)

instance H.Hashable Group

instance Show Group where
    show (Group g) = "Group:" <> show g

{- TODO: define DSL for Model -}

data Rule a
    = Target a
    | Cat PathHash Int (Rule a)
    | Rule PathHash Path (Rule a)
    deriving (Show, Read, Eq, Ord, Generic)

instance (H.Hashable a) => H.Hashable (Rule a) where
    hashWithSalt s (Target a) =
      s `H.hashWithSalt`
      (1 :: Int) `H.hashWithSalt` a
    hashWithSalt s (Cat h i r) =
      s `H.hashWithSalt`
      (2 :: Int) `H.hashWithSalt`
      h `H.hashWithSalt`
      i `H.hashWithSalt` r
    hashWithSalt s (Rule h t r) =
      s `H.hashWithSalt`
      (3 :: Int) `H.hashWithSalt`
      h `H.hashWithSalt`
      t `H.hashWithSalt` r

data Position a
    = Pos a
    | Other
    deriving (Read, Eq, Ord)

instance (Show a) => Show (Position a) where
    show (Pos a) = show "Pos:" <> show a
    show Other = "Other"

data ModifyRule
    = Expand   -- ^ normalization array element for db
    | List     -- ^ save list in db instead of normalization
    | Index (Position Int) -- ^ i th element
    | Stop T.Text          -- ^ stop parsing
    deriving (Read, Eq, Ord)

instance Show ModifyRule where
    show Expand = "Expand"
    show List = "List"
    show (Index a) = "Index:(" <> show a <> ")"
    show (Stop t) = "Stop:" <> show (T.unpack t)

data Storage
    = DS
    { database :: T.Text
    , table :: T.Text
    }
    deriving (Show, Read, Eq)

data VariableType
    = VObject (Maybe T.Text) (S.Set VariableType)
    | VArray (Maybe T.Text)
    | VText (Maybe T.Text)
    | VNumber (Maybe T.Text)
    | VBool (Maybe T.Text)
    | VNull (Maybe T.Text)
    deriving (Show, Read, Eq, Ord, Generic)

instance H.Hashable VariableType
instance (H.Hashable a) => H.Hashable (S.Set a) where
  hashWithSalt s a = H.hashWithSalt s $ S.toList a

data VDataType
    = VDArray VDataType
    | VDText
    | VDNumber
    | VDBool
    | VDNull
    deriving (Read, Eq, Ord, Generic)

instance Show VDataType where
    show (VDArray a) = "VDArray:" <> show a
    show VDText = "VDText"
    show VDNumber = "VDNumber"
    show VDBool = "VDBool"
    show VDNull = "VDNull"

rootHash :: BS.ByteString
rootHash = BB.encode "rh"

build :: Int  -- ^ total
      -> Int  -- ^ page
      -> Int  -- ^ number
      -> String -- ^ filename of model
      -> String -- ^ filename of config
      -> Env VariableType
      -> H.HashMap (Rule VariableType) [ BS.ByteString ]
      -> Storage
      -> Maybe Int  -- ^ limit number of ref doc
      -> [ Rule () ]
      -> String -- ^ source
      -> String -- ^ connect string
      -> String -- ^ connect string for read
      -> IO ()
build t p n m c e h d o l id cs csr = do
  pool <- connDb cs 3
  poolRead <- connDb csr 3
  build' t p n m c e h d o l id pool poolRead
  where build' t p n m c e h d o l id pool poolRead
          | t > 0 = do
            putStrLn $ "remaining doc for parsing .. " <> show t
            rs <- liftIO $ selectRestrict p n id poolRead

            let (rs', vs) = verifyJsonDecode rs

            mapM_ (putStrLn <=< jsonDecodeFail) rs'

            let (r, e') = smallBuild vs e l
                s' = H.map (limitNumIndex o) $ H.unionWith (<>) r h
            hFlush stdout
            seq s' build' (t - n) (p + 1) n m c e' s' d o l id pool poolRead

          | otherwise = do
            putStrLn "output model .."
            mHandler <- openFile m WriteMode
            hPutStrLn mHandler $ modelEnvOutput e
            hClose mHandler

            putStrLn "output config .."
            cHandler <- openFile c WriteMode
            hPutStrLn cHandler $ modelRuleOutput d h
            hClose cHandler

jsonDecodeFail :: RawData -> IO String
jsonDecodeFail r = return $ "Json decode failed: " <> show r

verifyJsonDecode :: [ RawData ] -> ( [ RawData ], [ (BS.ByteString, A.Value) ] )
verifyJsonDecode rs = (rs', vs')
    where decodeWith x =
            (x
            , ( BS.pack $ U.encode $ identity x
              , A.decodeStrict $ B.toStrict $ preprocessData x))
          decodeList :: [(RawData, (BS.ByteString, Maybe A.Value))]
          decodeList = map decodeWith rs
          vs' = map (fmap M.fromJust . snd) $ filter (M.isJust . snd . snd) decodeList
          rs' = map fst $ filter (M.isNothing . snd .snd ) decodeList

smallBuild
  :: [ (BS.ByteString, A.Value) ]
  -> Env VariableType
  -> [ Rule () ]
  -> (H.HashMap (Rule VariableType) [ BS.ByteString ], Env VariableType)
smallBuild vs e r = (dict , s)
  where parsingDoc = parsing rootHash r vs
        (a, s) = runState parsingDoc e
        dict = reverseDocIndex a

reverseDocIndex
  :: (Eq b, H.Hashable b, Show a, Show b)
  => [(a, b)]
  -> H.HashMap b [a]
reverseDocIndex rs
  = reverseDocIndex' rs H.empty
  where reverseDocIndex'
          :: (Eq b, H.Hashable b, Show a, Show b)
          => [(a, b)]
          -> H.HashMap b [a]
          -> H.HashMap b [a]
        reverseDocIndex' [] h = h
        reverseDocIndex' ((a, b):xs) h
          = reverseDocIndex' xs $ H.insertWith (<>) b [a] h

limitNumIndex :: Maybe Int -> [ a ] -> [ a ]
limitNumIndex (Just i) xs = take i xs
limitNumIndex Nothing xs = xs

parsing
  :: PathHash
  -> [ Rule () ]
  -> [ (BS.ByteString, A.Value) ]
  -> State (Env VariableType) [(BS.ByteString, Rule VariableType)]
parsing p r vs =
    concat <$> mapM (f . fmap (parsing' p r)) vs
    where f :: (Monad m) => (a,m [b]) -> m [(a, b)]
          f (x,ys) = do
              ys' <- ys 
              return (map (\y-> (x,y)) ys')

parsing'
  :: PathHash
  -> [ Rule () ]
  -> A.Value
  -> State (Env VariableType) [Rule VariableType]
parsing' h [Target ()] v = return [ Target $ VText Nothing ]
parsing' h r (A.Object o)
  = H.foldrWithKey replacement (return []) o
  where replacement k x y
          = (<>)
          <$> fmap (map (Rule (newH k) k)) (parsing'' k r x)
          <*> y
        bsKey k = BS.pack $ U.encode $ T.unpack k
        newH = newHash h . bsKey
        parsing'' k r = parsing' (newH k) (filterStopRule k r)

parsing' h r (A.Array a) =
  V.foldr replacement (return []) a
  where replacement x y = (<>) <$> parsing'' x <*> y
        parsing'' x = parsingArray x r h

parsing' h _ (A.String t) = return [Target $ VText Nothing]
parsing' h _ (A.Number s) = return [Target $ VNumber Nothing]
parsing' h _ (A.Bool b) = return [Target $ VBool Nothing]
parsing' _ _ A.Null = return [Target $ VNull Nothing]

parsingArray
  :: A.Value
  -> [ Rule () ]
  -> PathHash
  -> State (Env VariableType) [Rule VariableType]
parsingArray v r p = do
    env <- get

    let t = valueType v r Nothing
        g = lookupCat env p t
        newH = newHash p . bsKey
        bsKey i = BS.pack $ U.encode $ show i
        process i
          = map (Cat (newH i) i)
          <$>  parsing' (newH i) r v
    let gId = M.fromMaybe (countCat env p) g
    put $ addCat p t gId env
    process gId

filterStopRule :: Path -> [ Rule () ] -> [ Rule () ]
filterStopRule t [] = []
filterStopRule t (Rule _ p r : xs)
   | t == p = r : filterStopRule t xs
   | otherwise = filterStopRule t xs
filterStopRule t (x:xs) = filterStopRule t xs

lookupCat :: (Ord a, Eq a, H.Hashable a) => Env a -> PathHash -> a -> Maybe Id
lookupCat (Env e) p a = H.lookup p e >>= H.lookup a

countCat :: Env a -> PathHash -> Int
countCat (Env e) p = M.maybe 0 H.size $ H.lookup p e

addCat :: (Ord a, Eq a, H.Hashable a) => PathHash -> a -> Int -> Env a -> Env a
addCat p a i (Env e) = Env (H.insertWith H.union p (H.singleton a i) e)

newHash :: BS.ByteString -> BS.ByteString -> BS.ByteString
newHash h s = E.either undefined (newHash' s) $ BB.decode h
  where newHash' s r = BB.encode $ r <> "/" <> s

columnSetting :: Int -> [ TL.ColSpec ]
columnSetting i = replicate i TL.def

header :: TL.Row String
header = [ "Id"
         , "IsIndex"
         , "Group"
         , "Parent"
         , "ModifyRule"
         , "Type"
         , "Database"
         , "Table"
         , "Column"
         , "Path"
         , "UniqueKey"
         , "ReferenceDoc"
         , "Rule"
         ]

appendHeader :: [ TL.Row String ] -> [ TL.Row String ]
appendHeader c = header : c

modelRuleOutput
  :: Storage
  -> H.HashMap (Rule VariableType) [ BS.ByteString ]
  -> String
modelRuleOutput d ls = TL.gridString (columnSetting lenHeader)
                        $ appendHeader
                        $ genIdx
                        $ sortBy
                        $ filterNullType
                        $ map content
                        $ H.toList ls
  where content (l, i)
          = [ show True
          , show [Group 0]
          , show (Nothing :: Maybe Int)
          , show Expand
          , show $ getRuleType l
          , T.unpack $ database d
          , T.unpack $ table d
          , path l
          , path l
          , "uuid"
          , toBase64 i
          , toBase64 l
          ]
        lenHeader = length header
        sortBy = L.sortOn (!!7)
        filterNullType = filter ((/= show VDNull) . (!! 4))
        path = T.unpack . showRulePath

genIdx :: [[ String ]] -> [[ String ]]
genIdx = genIdx' 0
    where genIdx' :: Int -> [[ String ]] -> [[ String ]]
          genIdx' i [] = []
          genIdx' i (x:xs) =  (show i : x) : genIdx' (i + 1) xs

toBase64 :: (Show a) => a -> String
toBase64 = U.decode . BS.unpack . BB.encode . BS.pack . U.encode . show

fromBase64 :: String -> Either String String
fromBase64 = fmap (U.decode . BS.unpack) . BB.decode . BS.pack . U.encode

showRulePath :: Rule a -> T.Text
showRulePath (Target _) = ""
showRulePath (Cat _ i (Target _)) = T.pack (show i)
showRulePath (Cat _ i r) = T.pack (show i) <> "/" <> showRulePath r
showRulePath (Rule _ p (Target _)) = p
showRulePath (Rule _ p r) = p <> "/"  <> showRulePath r

getRuleType :: Rule VariableType -> VDataType
getRuleType (Target v) = toVDataType v
getRuleType (Cat _ _ r) = getRuleType r
getRuleType (Rule _ _ r) = getRuleType r

toVDataType :: VariableType -> VDataType
toVDataType (VObject _ _) = VDText
toVDataType (VArray _) = VDText
toVDataType (VText _) = VDText
toVDataType (VNumber _) = VDNumber
toVDataType (VBool _) = VDBool
toVDataType (VNull _) = VDNull

fromVDataType :: VDataType -> VariableType
fromVDataType VDText = VText Nothing
fromVDataType VDNumber = VNumber Nothing
fromVDataType VDBool = VBool Nothing
fromVDataType VDNull = VNull Nothing
fromVDataType (VDArray a) = fromVDataType a

modelEnvOutput :: Env VariableType -> String
modelEnvOutput = show

valueType
  :: A.Value
  -> [ Rule () ]
  -> Maybe T.Text
  -> VariableType
valueType v [Target ()] s = VText s
valueType (A.Object o) r s =
  VObject s
    $ H.foldrWithKey
      (\k v a -> S.insert (valueType v (filterStopRule k r) $ Just k) a)
      S.empty
      o
valueType (A.Array _) r s = VArray s
valueType (A.String _) r s = VText s
valueType (A.Number _) r s = VNumber s
valueType (A.Bool _) r s = VBool s
valueType A.Null r s = VNull s
