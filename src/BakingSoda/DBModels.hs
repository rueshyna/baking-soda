{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE DeriveDataTypeable         #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DeriveGeneric              #-}

module BakingSoda.DBModels where

import BakingSoda.UtilTH

import Database.Groundhog
import Database.Groundhog.TH
import Database.Groundhog.Postgresql
import Database.Groundhog.Postgresql.Array as G

import Data.ByteString.Lazy as B
import Data.Time.Clock

import GHC.Generics (Generic)

data RawData = RawData
             { identity          :: String
             , source            :: String
             , rawData           :: B.ByteString
             , preprocessData    :: B.ByteString
             , md5               :: String
             , insertTimestamp   :: UTCTime
             } deriving (Show, Generic)

mkPersist codegenConfig [groundhog|
definitions:
entity: RawData
keys:
  - name: uniqueIdentity
constructors:
  - name: RawData
    uniques:
      - name: uniqueIdentity
        fields: [ identity ]
      - name: index
        type: index
        fields: [ identity, source, md5 ]
|]
